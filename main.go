package dodos_module_3

import (
	"fmt"
	"math"
)

// v1.0.0
func Concat(firstName string, lastName string) (string, string) {
	return fmt.Sprintf("%s %s", firstName, lastName), string(firstName[0])
}

// v1.0.1
func GetSolveQuadraticEquation(a float64, b float64, c float64) (x1 float64, x2 float64) {
	var d float64 = b*b - 4*a*c
	x1 = (-b + math.Sqrt(d)) / 2*a
	x2 = (-b - math.Sqrt(d)) / 2*a
	return x1, x2
}

// v1.0.2
func ShiftBits(num int, isRight bool, numToShift int) int {
	if isRight {
		return num >> numToShift
	} else {
		return num << numToShift
	}
}